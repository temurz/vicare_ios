//
//  GetCardConfigUseCase.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 22/12/21.
//  Copyright © 2021 Tuan Truong. All rights reserved.
//

protocol GetCardConfigUseCaseType {
    func getCardConfig(configCode: String)->Observable<CardConfig>
}

struct GetCardConfigUseCase: GetCardConfigUseCaseType, GettingCardConfig {
    let cardConfigGateway: CardConfigGatewayType
    
}
