//
//  LoginNavigator.swift
//  CleanArchitecture
//
//  Created by Tuan Truong on 7/14/20.
//  Copyright © 2020 Tuan Truong. All rights reserved.
//
import UIKit

protocol LoginNavigatorType {
    func showMainOrBack(cardConfig: CardConfig)
}

struct LoginNavigator: LoginNavigatorType, ShowingMain {
    func showMainOrBack(cardConfig: CardConfig) {
        if navigationController.viewControllers.count == 1 {
            showMain(cardConfig: cardConfig)
        } else {
            navigationController.popViewController(animated: true)
        }
      
    }
    
    unowned let assembler: Assembler
    unowned let navigationController: UINavigationController
}
