//
//  LoginView.swift
//  CleanArchitecture
//
//  Created by Tuan Truong on 7/29/20.
//  Copyright © 2020 Tuan Truong. All rights reserved.
//

import SwiftUI
import Combine

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

struct LoginView: View {
    @ObservedObject var input: LoginViewModel.Input
    @ObservedObject var output: LoginViewModel.Output
    @State var focused: [Bool] = [true, false]
    @State var severalCards: Bool = false
    
    private let cancelBag = CancelBag()
    private let loginTrigger = PassthroughSubject<Void, Never>()
    private let languageTrigger = PassthroughSubject<String,Never>()
    
    var body: some View {
        
        
        return LoadingView(isShowing: $output.isLoading, text: .constant("")) {
            VStack {
                WelcomeView()
                Spacer()
                VStack(alignment: .leading){
                    if severalCards{
                        Menu("Услуги".localized()) {
                            Picker(selection: $output.serviceType) {
                                
                            } label: {
                                
                            }

                        }
                    }
                    
                    TextFieldTyped(keyboardType: .default, returnVal: .next, tag: 0,   isSecure: false, text: $input.username, isfocusAble: $focused)
                        .placeholderText(when: input.username.isEmpty, placeholder: {
                            Text("Certificate".localized())
                                .foregroundColor(.gray)
                        })
                        .padding()
                        .background(lightGreyColor)
                        .frame(height: 50)
                        .cornerRadius(5.0).onTapGesture {
                            self.focused = [true, false]
                        }
                    

                    Text(self.output.usernameValidationMessage)
                        .foregroundColor(.red)
                        .font(.footnote)
                        .lineLimit(nil)
                        .frame(maxHeight: .infinity)
                        .fixedSize()
                }.padding(.bottom,15)
                VStack(alignment: .leading){
                    TextFieldTyped(keyboardType: .default, returnVal: .done, tag: 1, isSecure: true, text: $input.password, isfocusAble: $focused)
                        .placeholderText(when: input.password.isEmpty, placeholder: {
                            Text("Password".localized())
                                .foregroundColor(Color.gray)
                        })
                        .padding()
                        .background(lightGreyColor)
                        .frame(height: 50)
                        .cornerRadius(5.0).onTapGesture {
                            self.focused = [false, true]
                        }
                    Text(self.output.passwordValidationMessage)
                        .foregroundColor(.red)
                        .font(.footnote)
                        .lineLimit(nil)
                        .frame(maxHeight: .infinity)
                        .fixedSize()
                }.padding(.bottom,15)
                HStack{
                    Spacer()
                    Button(action: {self.loginTrigger.send(())}) {
                        Text("Login".localized())
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 220, height: 60)
                            .background(Color.green)
                            .cornerRadius(15.0)
                        
                    } .disabled(!self.output.isLoginEnabled)
                    Spacer()
                }
                HStack{
                    Text("Ru")
                        .padding()
                        .font(.system(size: 18))
                        .onTapGesture {
                            self.languageTrigger.send("ru")
                            input.change = "123"
                        }
                    
                    Text("Uz")
                        .padding()
                        .font(.system(size: 18))
                        .onTapGesture {
                            self.languageTrigger.send("uz")
                            input.change = "123"
                        }
                    
                }
                Spacer()
                
            }
            .padding()
        }
        .navigationTitle(Text("Login".localized()))
        .alert(isPresented: $output.alert.isShowing) {
            Alert(
                title: Text(output.alert.title),
                message: Text(output.alert.message),
                dismissButton: .default(Text("OK"))
            )
        }
    }
    
    init(viewModel: LoginViewModel) {
        let input = LoginViewModel.Input(loginTrigger: loginTrigger.asDriver(), languageTrigger: languageTrigger.asDriver())
        output = viewModel.transform(input, cancelBag: cancelBag)
        self.input = input
    }
}
struct WelcomeView : View {
    var body: some View {
        return Image("logo")
            .resizable()
            .scaledToFit()
            .frame(width: 200, height: 220)
        
    }
}



struct TextFieldTyped: UIViewRepresentable {
    let keyboardType: UIKeyboardType
    let returnVal: UIReturnKeyType
    let tag: Int
    let isSecure: Bool
    @Binding var text: String
    @Binding var isfocusAble: [Bool]

    func makeUIView(context: Context) -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.keyboardType = self.keyboardType
        textField.returnKeyType = self.returnVal
        textField.tag = self.tag
        textField.delegate = context.coordinator
        textField.autocorrectionType = .no
        textField.isSecureTextEntry = isSecure
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        if !isfocusAble[tag]{
            uiView.resignFirstResponder()
            return
        }
        
        if !uiView.isFirstResponder {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                uiView.becomeFirstResponder()
            }
        }
        
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UITextFieldDelegate {
        var parent: TextFieldTyped

        init(_ textField: TextFieldTyped) {
            self.parent = textField
        }
      
       
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            // Without async this will modify the state during view update.
            DispatchQueue.main.async {
                self.parent.text = textField.text ?? ""
            }
        }

        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            var focus = self.parent.isfocusAble
            
            if parent.tag == 0 {
                
                focus = [false, true]
            } else if parent.tag == 1 {
                focus = [false, false]
            }
            DispatchQueue.main.async {
                self.parent.isfocusAble = focus
            }
        return true
        }
    }
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel: LoginViewModel = PreviewAssembler().resolve(navigationController: UINavigationController(),  cardConfig: CardConfig(configCode: "AAOo2"))
        return LoginView(viewModel: viewModel)
    }
}

extension View {
    func placeholderText<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {

        ZStack(alignment: alignment) {
            placeholder().opacity(shouldShow ? 1 : 0)
            self
        }
    }
}
