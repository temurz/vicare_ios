//
//  APIUrls.swift
//  CleanArchitecture
//
//  Created by Tuan Truong on 7/31/20.
//  Copyright © 2020 Tuan Truong. All rights reserved.
//

extension API {
    enum Urls {
        static let login = "http://airfel.akfa.uz/api/v2" + "/auth/login"
        static let logout = "http://airfel.akfa.uz/api/v2" + "/auth/logout"
        static let currentUser = "http://airfel.akfa.uz/api/v2"  + "/auth/current"
        static let changePassword = "http://airfel.akfa.uz/api/v2"  + "/master/change/password"
        static let events = "http://airfel.akfa.uz/api/v2" + "/events/list"
        static let eventTypes = "http://airfel.akfa.uz/api/v2" + "/events/types"
        static let agentCurrentICU = "http://airfel.akfa.uz/api/v2" + "/master/balance"
        static let addToCard = "http://airfel.akfa.uz/api/v2" + "/order/entry/add"
        static let currentShoppingCart = "http://airfel.akfa.uz/api/v2" + "/order/current"
        static let productCategories = "http://airfel.akfa.uz/api/v2" + "/product/categories/"
        static let products = "http://airfel.akfa.uz/api/v2"  + "/products"
        static let eventDetail = "http://airfel.akfa.uz/api/v2" + "/events/%d"
        static let transactionHistory =  "http://airfel.akfa.uz/api/v2" + "/scores/history"
        static let transactionTypes = "http://airfel.akfa.uz/api/v2" + "/scores/types"
        static let transactionStatistic = "http://airfel.akfa.uz/api/v2" + "/scores/total"
        static let updateProductEntry = "http://airfel.akfa.uz/api/v2" + "/order/entry/update"
        static let deleteProductEntry = "http://airfel.akfa.uz/api/v2" + "/order/entry/delete"
        static let clearProductEntries = "http://airfel.akfa.uz/api/v2" + "/order/entry/clear"
        static let checkout = "http://airfel.akfa.uz/api/v2" + "/order/checkout"
        static let cancelOrder = "http://airfel.akfa.uz/api/v2" + "/order/cancel"
        static let getOrders = "http://airfel.akfa.uz/api/v2" + "/order/history"
        static let getOrderStatuses = "http://airfel.akfa.uz/api/v2" + "/order/statuses"
        static let getOrder = "http://airfel.akfa.uz/api/v2" + "/order/"
        static let getNotifications = "http://airfel.akfa.uz/api/v2" + "/notification/list"
        static let getCardsHistory = "http://airfel.akfa.uz/api/v2" + "/cards/history"
        static let getCardsDetail = "http://airfel.akfa.uz/api/v2" + "/cards/history/{id}"
        static let sendFcmToken = "http://airfel.akfa.uz/api/v2" + "/auth/registerFcmId"
    }
}
