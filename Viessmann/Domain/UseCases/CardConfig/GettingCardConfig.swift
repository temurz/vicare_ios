//
//  GettingCardConfig.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 22/12/21.
//  Copyright © 2021 Tuan Truong. All rights reserved.
//

import Combine

protocol GettingCardConfig {
    var cardConfigGateway: CardConfigGatewayType { get }
}

extension GettingCardConfig {
    func getCardConfig(configCode: String) -> Observable<CardConfig> {
        cardConfigGateway.getCardConfig(configCode: configCode)
    }
}

