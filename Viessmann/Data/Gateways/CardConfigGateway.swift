//
//  CardConfigGateway.swift
//  CleanArchitecture
//
//  Created by Damir Asamatdinov on 22/12/21.
//  Copyright © 2021 Tuan Truong. All rights reserved.
//

import Combine
import Foundation

protocol CardConfigGatewayType {
    func getCardConfig(configCode:String) -> Observable<CardConfig>
    func getCardConfigs() -> Observable<[CardConfig]>
}


struct CardConfigGateway: CardConfigGatewayType {
    func getCardConfigs() -> Observable<[CardConfig]> {
        Future<[CardConfig], Error> { promise in
            
            let  steps = [
                AddCardStep(id: 1, autoComplete: true, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Серийный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_BARCODE, minLength: 0, maxLength: 0, cardStepId: 1, hasValidationError: false, titleUz: "Seriya raqami", skip: false, productionCode: nil, limit: 0, editable: false, valueString: "", originaltemId: nil)], titleUz: "Seriya raqami", titleRu: "Серийный номер", cardConfig: nil),
                AddCardStep(id: 2, autoComplete: false, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Телефонный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_PHONE, minLength: 0, maxLength: 0, cardStepId: 2, hasValidationError: false, titleUz: "Telefon raqami", skip: false, productionCode: nil, limit: 0, editable: true, valueString: "", originaltemId: nil)], titleUz: "Mijoz", titleRu: "Клиент", cardConfig: nil)
            ]
            
            var config = CardConfig(configCode: "AA001", configVersion: nil, canRequest: false, showConfirmation:false, titleRu: "Продажа (котел Airfel)", titleUz: "Sotish (Airfel qozoni)", minExchange: 0, hasPayment: false, steps: steps, remoteUrl: "http://airfel.akfa.uz/api/v2", hasShop: true, iconUrl: nil, installedDate: Date(), longitude: nil, latitude: nil)
            
            if let remoteURL = config.remoteUrl{
                config.iconUrl = String(format: "%@/image/config/%@.jpg", remoteURL, config.configCode)
            }
            
            promise(.success([config]))
        }
        .eraseToAnyPublisher()
    }
    
    func getCardConfig(configCode:String) -> Observable<CardConfig> {
        Future<CardConfig, Error> { promise in
            
            let  steps = [
                AddCardStep(id: 1, autoComplete: true, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Серийный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_BARCODE, minLength: 0, maxLength: 0, cardStepId: 1, hasValidationError: false, titleUz: "Seriya raqami", skip: false, productionCode: nil, limit: 0, editable: false, valueString: "", originaltemId: nil)], titleUz: "Seriya raqami", titleRu: "Серийный номер", cardConfig: nil),
                AddCardStep(id: 2, autoComplete: false, items: [AddCardStepItem(id: UUID().uuidString, titleRu: "Телефонный номер", value: nil, remoteName: nil, type: AddCardStepItemType.EDIT_TEXT_PHONE, minLength: 0, maxLength: 0, cardStepId: 2, hasValidationError: false, titleUz: "Telefon raqami", skip: false, productionCode: nil, limit: 0, editable: true, valueString: "", originaltemId: nil)], titleUz: "Mijoz", titleRu: "Клиент", cardConfig: nil)
            ]
            
            var config = CardConfig(configCode: "AA001", configVersion: nil, canRequest: false, showConfirmation:false, titleRu: "Продажа (котел Airfel)", titleUz: "Sotish (Airfel qozoni)", minExchange: 0, hasPayment: false, steps: steps, remoteUrl: "http://airfel.akfa.uz/api/v2", hasShop: true, iconUrl: nil, installedDate: Date(), longitude: nil, latitude: nil)
            
            if let remoteURL = config.remoteUrl{
                config.iconUrl = String(format: "%@/image/config/%@.jpg", remoteURL, configCode)
            }
            
            promise(.success(config))
        }
        .eraseToAnyPublisher()
    }
}

struct PreviewCardConfigGateway: CardConfigGatewayType {
    func getCardConfigs() -> Observable<[CardConfig]> {
        Future<[CardConfig], Error> { promise in
            
            promise(.success([CardConfig(configCode: "AA002")]))
        }
        .eraseToAnyPublisher()
    }
    
    func getCardConfig(configCode:String) -> Observable<CardConfig> {
        Future<CardConfig, Error> { promise in
            
            promise(.success(CardConfig(configCode: "AA002")))
        }
        .eraseToAnyPublisher()
    }
    
}

